#Rational Number class (c)2013 Charles Fox licened under GPL

from fractions import gcd
from math import floor
#TODO negative rationals!

class Rational:
	def __init__(self, num, den):
		self.num=num
		self.den=den
		self.simplify()
	def simplify(self):
		g = gcd(self.num, self.den)
		self.num /= g
		self.den /= g
	def __mul__(self, other):
		num = self.num*other.num
		den = self.den*other.den
		return Rational(num,den)
	def __div__(self,other):
		num = self.num*other.den
		den = self.den*other.num
		return Rational(num,den)
	def __add__(self,other):
		if type(other)==int:
			other = Rational(other,1)
		num = (self.num*other.den)+(other.num*self.den)
		den = (self.den*other.den)
		return Rational(num,den)
	def __radd__(self,other):      #required for functions like sum() to find right kind of addition
		if type(other)==int:
			other = Rational(other,1)
		num = (self.num*other.den)+(other.num*self.den)
		den = (self.den*other.den)
		return Rational(num,den)
	def __rsub__(self,other):      #required for functions like sum() to find right kind of addition
		if type(other)==int:
			other = Rational(other,1)
		num = (self.num*other.den)-(other.num*self.den)
		den = (self.den*other.den)
		return Rational(num,den)
	def __sub__(self,other):
		num = (self.num*other.den)-(other.num*self.den)
		den = (self.den*other.den)
		return Rational(num,den)
	def __str__(self):
		return str(self.num)+"/"+str(self.den)
	def toFloat(self):
		return (0.0+self.num)/self.den		
	def floor(self):
		return int(floor(self.toFloat()))
	def __hash__(self):   #for use as dct key
        	return hash((self.num, self.den))
	def __eq__(self, other):   #for dct key comparison
		if not other.__class__==Rational:
			return False   #TODO what about ints? floats?
        	return (self.num, self.den) == (other.num, other.den)
	def __lt__(self,other):    #less than
		mynum = (self.num*other.den)
		othernum = (other.num*self.den)
		return (mynum<othernum)
	def __gt__(self,other):    #less than
		mynum = (self.num*other.den)
		othernum = (other.num*self.den)
		return (mynum>othernum)
	def __le__(self,other):    #less than
		mynum = (self.num*other.den)
		othernum = (other.num*self.den)
		return (mynum<=othernum)
	def __ge__(self,other):    #less than
		mynum = (self.num*other.den)
		othernum = (other.num*self.den)
		return (mynum>=othernum)

def pRatDct(dct):
	for key in sorted(dct.keys()):
		print key.__str__()+":"+dct[key].__str__() 
	
r1 = Rational(2,1)
r2 = Rational(1,1)
print r1 <= r2
