
import ipdb as pdb
import os,re,sys,subprocess,math,random,copy
sys.path.insert(0, './lib')
from CFMidiFile  import *

#make a .mid file that sends controller 123 to each channel (panic)

def off():
        myMIDI = MIDIFile(1)        # Create the MIDIFile Object with 1 track
        track = 0                   # Tracks are numbered from zero. Times are measured in beats.
        time = 0
        myMIDI.addTrackName(track,time,"MusicHastieTrack1")              # Add track name and tempo.
        myMIDI.addTempo(track,time,120)   #was 120
	for channel in range(0,4):   #py ch start at 0
                track = 0                   # Add a note. addNote expects the following information:
                time = 0
                controlParam = 123   #these are dict keys.  check if any control params change -- if so, make new msgs from them 
		paramValue = 0
                myMIDI.addControllerEvent(track, channel,time,controlParam, paramValue)   #decrement time a little so happens just BEFORE its note

        binfile = open("off.mid", 'wb')                          #write to disc
        myMIDI.writeFile(binfile)
        binfile.close()

off()
